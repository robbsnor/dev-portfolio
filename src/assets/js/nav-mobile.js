
var navIsopen = false;
var animationIsComplete = true;

var backdrop = '.nav-mobile__backdrop';
var navItems = '.nav-mobile__item';



function navOverlayToggle() {
    if (animationIsComplete === true) {
        if (navIsopen === true) {
            navIsopen = false;
            navOverlayclose();
        } else {
            navIsopen = true;
            navOverlayOpen();
        }
    }
  }

  $(".navToggle").on("click", function () {
    navOverlayToggle();
  });



function navOverlayOpen() {
    animationIsComplete = false;

    var tl = anime.timeline({
        easing: 'easeInOutExpo',
        targets: backdrop,
    });

    tl
        .add({
            top: ['-100%', 0],
            left: ['-100%', 0],
            scale: 0,
            duration: 1
        })
        .add({
            scale: [.5, 1],
            opacity: 1,
            duration: 500
        })
        .add({
            targets: navItems,
            translateY: [20, 0],
            opacity: [0, 1],
            delay: anime.stagger(100)
        }, '-=500')
        .add({
            complete: function (anim) {
                animationIsComplete = true;
            }
        }, 0)

}

function navOverlayclose() {
    animationIsComplete = false;

    var tl = anime.timeline({
        easing: 'easeInOutExpo',
        targets: backdrop,
    });

    tl
        .add({
            targets: navItems,
            translateY: 20,
            opacity: 0,
            duration: 500,
            delay: anime.stagger(100)
        })
        .add({
            scale: .5,
            opacity: 0,
            duration: 500
        }, '-=250')
        .add({
            top: '-100%',
            left: '-100%',
            scale: 1,
            duration: 1
        })
        .add({
            complete: function (anim) {
                animationIsComplete = true;
            }
        }, 0)
}



